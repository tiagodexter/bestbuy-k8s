Best Buy Assignment


# Goals
- Automate the deployment process to reduce human interaction and points of failure
- Deploy different applications based on its variables
- Deploy application between environments
- Create documentation explaining how it was done.

# Workflow
![Chart](Chart.png)

# History
I decided to deploy all systems using AWS and Kubernetes. 
I started using EKS, but after test CodePipeline I couldn't find a way to deploy to a kubernetes cluster, so changed to Kops.
Because of that, I decided to use multi-cloud systems, such as AWS and Attlasian Bitbucket.

- DNS: Route 53
- Kubernetes: Kops
- GIT: BitBucket
- Pipeline: Bitbucket Pipeline
- Load Balance: AWS Load Balance

# Procedures to connect on 

To configure all environment to access the k8s, please follow these steps:

1: Install kubectl

```
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
```
2: Install Kops

```
curl -LO https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64
chmod +x kops-linux-amd64
sudo mv kops-linux-amd64 /usr/local/bin/kops
```
4: Install and configure AWS Cli
If Debian flavor
```
sudo apt-get install aws-cli
```
If Red Hat flavor
```
sudo yum install aws-cli
```
So now just configure the access to AWS using the command ```aws configure``` and add the AWS Secret and AWS Key

Put the config sent into ```~/.kube/config``` file

After this, your kubectl, kops  and your ```~/.kube/config``` will be configured and working.
I also implemented the Kubernetes Dashboard, so if you want, just use kubectl proxy to access it.

# Pipeline
I created these steps on Bitbucket Pipeline:
- Build the project 
- Build the Docker image and send to ECR 
- Deploy to development environment 
- Deploy to test environment (Manually) 
- Deploy to dr and prod environment (Manually) 
The file contains these configuration is in [bitbucket-pipelines.yml](https://bitbucket.org/tiagodexter/bestbuy/src/develop/bitbucket-pipelines.yml) on the other repository

# Informations
### Docker
  - [https://docs.docker.com/install/](https://docs.docker.com/install/)
### ECR
  - The tag of the image is the same of the ID of the commit on Bitbucket
  - [https://aws.amazon.com/pt/ecr/](https://aws.amazon.com/pt/ecr/)
  - ![ECR](ecr.png)
### Route 53
  - [https://aws.amazon.com/pt/route53/](https://aws.amazon.com/pt/route53/)
  - ![Route configuration](route53.png)
### Repositories
  - [https://bitbucket.org/tiagodexter/bestbuy](https://bitbucket.org/tiagodexter/bestbuy)
  - [https://bitbucket.org/tiagodexter/bestbuy-k8s/src/master/](https://bitbucket.org/tiagodexter/bestbuy-k8s/src/master/)
### Environments - FQDN
  - [https://dev.bestbuy.tiagodrumond.com.br](https://dev.bestbuy.tiagodrumond.com.br)
  - [https://test.bestbuy.tiagodrumond.com.br](https://test.bestbuy.tiagodrumond.com.br)
  - [https://dr.bestbuy.tiagodrumond.com.br](https://dr.bestbuy.tiagodrumond.com.br)
  - [https://prod.bestbuy.tiagodrumond.com.br](https://prod.bestbuy.tiagodrumond.com.br)
  - ![Environments](environments.png)
### Kubernetes result
  - ![Ḱubernetes](kubectl.png)
### Pipeline result
  - [https://bitbucket.org/tiagodexter/bestbuy/addon/pipelines/home](https://bitbucket.org/tiagodexter/bestbuy/addon/pipelines/home)
  - ![Pipeline Running](pipeline-running.png)
  - ![Pipeline](pipeline.png)
  - ![Variables](variables.png)
### EC2 Instances
  - ![Instances](instances.png)
### Load Balancer
  - All requests received on port 80 will be redirected to HTTPS
  - ![ELB](elb.png)